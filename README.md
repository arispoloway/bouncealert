### Bounce Alert ###

This SourceMod plugin is more or less an extension of bcheck, but it is approached in a very different way. This plugin allows for you to check for a possible bounce, crouched or uncrouched, while you are moving in the air. It does not take the height that you are standing at and determine what types of bounces you can do (ex. ctap uncrouch, walk off crouch) like bcheck does.

This plugin takes your velocity, position, and the position of a surface, and calculates in real time what bounces are possible, if any.

You can check for the bounces in 3 ways:

* Registered panels: Using the ba_reg or ba_register command while pointing at a surface, you can register that height. Then, to determine if you can bounce off of it, use ba_man or ba_manual. To delete the registered panel, use ba_del or ba_delete. This I limited because each user can only have one registered height at a time and you have to ba_reg each time, but it still might have some purpose.
* Directly beneath you: The command ba_below or ba_bel will let you know if you are capable of bouncing off of the platform below you.
* Where you are aiming: The command ba_loc or ba_location will let you know if you can bounce off of the platform you are looking at.

Just bind whichever these commands you find most useful to an unused mouse button or whatever you feel like and press the bind while you are moving following the instructions above and the plugin will tell you in chat "CROUCHED", "UNCROUCHED", or "CROUCHED OR UNCROUCHED".