/*
Uses SideX's traceray method
Thanks to Jamien for the spreadsheet of bounce values
Without that I probably couldn't have figured out TF2 physics

Also - I switched between the SourcePawn recommended code formatting and what I'm used to halfway through.
	 - Sorry for the shitty code that may result
*/



#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
	name = "Bounce Alert",
	author = "talkingmelon",
	description = "Will determine if a bounce is possible on a surface with varying initial velocity",
	version = ".3",
	url = "http://www.tf2rj.com"
};

//Initialize arrays to store and manage registered bounce heights
new players[32];
new playersLength = 32;
new bouncePlatforms[32];


public OnPluginStart(){

	PrintToServer("Initializing Bounce Alert");
	RegConsoleCmd("ba_register", Command_BounceAlertRegister, "Registers a bounce height");
	RegConsoleCmd("ba_delete", Command_BounceAlertDisable, "Deletes your registered Panel");
	RegConsoleCmd("ba_reg", Command_BounceAlertRegister, "ba_register - Registers a bounce height");
	RegConsoleCmd("ba_del", Command_BounceAlertDisable, "ba_delete - Deletes your registered Panel");
	RegConsoleCmd("ba_man", Command_BounceRegisteredNotify, "ba_manual - Checks bounces for the registered platform");
	RegConsoleCmd("ba_manual", Command_BounceRegisteredNotify, "Checks bounces for the registered platform");
	RegConsoleCmd("ba_bel", Command_BounceBelowNotify, "ba_below - Checks for bounces on the panel beneath you");
	RegConsoleCmd("ba_below", Command_BounceBelowNotify, "Checks for bounces on the panel beneath you");
	RegConsoleCmd("ba_loc", Command_BounceLocationNotify, "ba_location - Checks for bounces where you are pointing");
	RegConsoleCmd("ba_location", Command_BounceLocationNotify);

}

//Resets the registered platforms on a map change
public OnMapStart(){
	new i;
	for(i = 0; i < playersLength; i++){
		players[i] = 0;
		bouncePlatforms[i] = 0;
	}
}

//Checks for bounces on the registered panel
public Action:Command_BounceRegisteredNotify(client, args){
	//duh
	if( !client ){
		ReplyToCommand(client, "[SM] Cannot check bounces from rcon");
		return Plugin_Handled;
	}
	
	determineBounces(client, 1);
	return Plugin_Handled;
}

//Detects bounces directly beneath you
public Action:Command_BounceBelowNotify(client, args){
	//duh
	if( !client ){
		ReplyToCommand(client, "[SM] Cannot check bounces from rcon");
		return Plugin_Handled;
	}
	
	determineBounces(client, 2);
	return Plugin_Handled;
}

//Detects bounces where you are aiming
public Action:Command_BounceLocationNotify(client, args){
	//duh
	if( !client ){
		ReplyToCommand(client, "[SM] Cannot check bounces from rcon");
		return Plugin_Handled;
	}
	
	determineBounces(client, 3);
	return Plugin_Handled;
}




/*
Determines which bounces are possible given a client and a mode
Modes:
1 - Based on registered bounce height
2 - Based on the panel directly beneath you
3 - Based on the panel you are looking at
*/
determineBounces(client, mode){
	new id = GetClientUserId(client);
	new i;
	new bool:toCheck;
	
	//Checks if the user has registered a panel
	for(i = 0; i < playersLength;i++){
		if(players[i] == id){
		toCheck = true;
		break;
		}
	}
	
	//If they were in the list or they weren't using a registered panel but rather modes 2 or 3
	if(toCheck || mode != 1){
	
		decl Float:clientPos[3];
		decl Float:velocity[3];
		decl Float:feet[3];
		
		decl Float:diff;
		
		decl Float:p;
		decl Float:v;
		decl b;
		
		
		GetClientEyePosition(client, clientPos);
		
		//Velocity vector
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", velocity);
		
		//Position of feet
		GetEntPropVector(client, Prop_Data, "m_vecOrigin", feet);
		
		//PrintToServer("############");
		//PrintToServer("Vel: %f",velocity[2]);
		//PrintToServer("Ori: %f",feet[2]);
		//PrintToServer("Eye: %f",clientPos[2]);
		//PrintToServer("############");
		
		//Difference between heights of the feet and the eyes - the normal difference (uncrouched separation)
		//On land this can be anywhere from 0-23 but in the air it is either 0 or 23 depending on if you are crouching or not
		diff = feet[2]-clientPos[2]+68.0;
		
		//The height of the client
		p = clientPos[2];
		
		//The up/down velocity of the client
		v = velocity[2];
		
		//Uses the mode value to determine how it calculates the bounce
		//b is the value for the height of the bounce platform
		switch(mode){
			case 1:{
				b = bouncePlatforms[i];
			}
			case 2:{
				b = RoundToFloor(GetEndPosition(client, true));
				
			}
			case 3:{
				b = RoundToFloor(GetEndPosition(client, false));
			}
		
		
		}
		
		new crouchedBounce;
		new uncrouchedBounce;
		
		//If the client isn't crouching or half crouching
		//ALSO - I'm still not completely sure all of this works all the time because these lucky bounces are really hard to test
		//     - If people could make sure the bounce work like the plugin says the should that would be great
		//I'm not sure I could work myself through the math of this(the whole plugin) again, so try to figure it out yourself 
		if(diff != 0){
		
			//If the client is standing on the ground
			if(v==0.0){
				crouchedBounce = checkCrouchedBounce(feet[2]+48,b,v);
				uncrouchedBounce = checkUncrouchedBounce(feet[2]+48,b,v);
			}else{
				/*
				You might be wondering why there is a +3 after the p in these statements
				Turns out, when you are in the air and you crouch, your eyes go down 3 units and your feet go up 20 instead of your
					feet just coming up 23
				*/
				crouchedBounce = checkCrouchedBounce(p+3.0,b,v);
				uncrouchedBounce = checkUncrouchedBounce(p+3.0,b,v);
			}
			
		//If the client isn't crouching at all
		}else{
			crouchedBounce = checkCrouchedBounce(p,b,v);
			uncrouchedBounce = checkUncrouchedBounce(p,b,v);
		}
	
		
		
		//Outputs to chat whether or not the bounce is possible
		if(crouchedBounce && !uncrouchedBounce){
			PrintToChat(client, "CROUCHED");
		}else if(!crouchedBounce && uncrouchedBounce){
			PrintToChat(client, "UNCROUCHED");
		}else if(crouchedBounce && uncrouchedBounce){
			PrintToChat(client, "CROUCHED OR UNCROUCHED");
		}	
	}
}



//Given eye position (air crouching is compensated for above), velocity, and height, this determines if a crouched bounce is possible
//I could have combined this with the uncrouched bounce checker and saved alot of lines but I'm not about to do that right now
checkCrouchedBounce(Float:height, panel_height, Float:velocity){

	//Your height when the crouch is taken into account
	new Float:pos = height-48.0;
	
	//Gravity in TF2 on a server with 66.66666 tickrate is -.18 hu/tick
	new Float:grav = 0.18;
	new Float:vel;
	
	/*
	Note - When I say velocity below I mean the velocity per tick, not the velocity per second
	I calculated velocity per tick by dividing velocity per tick by 66.6666666, the tickrate
	
	I'm not entirely sure why this part works but it's really important
	
	TF2 calculates position by taking the velocity per tick of the last frame averaged with the velocity of the next frame and using
		that to calculate position. Since the velocity decreases by .18 each tick, the average is just the
		previous tick's velocity - .09
	
	
	*/
	if(velocity == 0){
		//This is the part that confuses me but it works
		//If someone can prove this wrong please explain it to me
		vel = 0.18;
	}else{
		//This gets the velocity per tick, and then adds .09 to help with averages
		//The position is calculated after gravity is applied, so the first velocity used by the loop will actually be .09 less than 
		//	the velocity per tick, not .09 more than it as it appears here
		vel = velocity/66.666666 + 0.09;
	}
	
	//Whether the bounce is possible or not
	new bounce = 0;
	
	
	//Tests for bounces by doing physics and things
	//Goes past the panel 50 units just because
	while(pos + 50 > panel_height){
	
		//Changes velocity according to the gravity per tick
		vel -= grav;
		
		//Calculates the new position based on the new average velocity
		pos = pos + vel;
		
		//Determines if the bounce is possible, and if so, breaks the loop
		if(RoundToFloor(pos) == panel_height+1){
			bounce = 1;
			break;
		}
		
	}
	return bounce;
}

//Given eye position (air crouching is compensated for above), velocity, and height, this determines if a crouched bounce is possible
checkUncrouchedBounce(Float:height, panel_height, Float:velocity){
	//SEE THE CROUCH FUNCTION FOR DETAILS
	
	//The only thing different between this and the crouched function is the 68 vs 45
	new Float:pos = height-68.0;
	new Float:grav = 0.18;
	new Float:vel;
	
	if(velocity == 0){
		vel = 0.18;
	}else{
		vel = 0.09 + velocity/66.666666;
	}
	
	new bounce = 0;
	while(pos + 50 > panel_height){
		vel -= grav;
		pos = pos + vel;
		if(RoundToFloor(pos) == panel_height+1){
			bounce = 1;
			break;
		}
		
	}
	return bounce;
}

//Removes the client from list of registered bounce heights if they had registered a panel
public OnClientDisconnect(client){
	
	//If the client had a registered height it removes it and the user
	new id = GetClientUserId(client);
	new i;
	for(i = 0; i < playersLength;i++){
		if(players[i]==id){
			players[i] = 0;
			bouncePlatforms[i] = 0;
		}
	}
}


//Registers the bounce height to allow for manual checking for bounces on registered platforms
public Action:Command_BounceAlertRegister(client,args){

	//duh
	if( !client ){
		ReplyToCommand(client, "[SM] Cannot check bounces from rcon");
		return Plugin_Handled;
	}
	
	
	decl roundHeight;
	decl Float:exactHeight
	
	//Find the height of the panel rounded down
	exactHeight = GetEndPosition(client, false);
	roundHeight = RoundToFloor(Float:exactHeight);
	
	new id = GetClientUserId(client);
	new i;
	new bool:inlist = false;
	
	//If the user is already in the list it registers the new height value
	for(i = 0; i < playersLength;i++){
		if(players[i]==id){
			bouncePlatforms[i] = roundHeight;
			inlist = true;
			break;
		}
	}
	
	//If the user is not already in the list it adds them to the list with the new height value
	if(!inlist){
		for(i = 0; i < playersLength;i++){
			if(players[i]==0){
				players[i] = id;
				bouncePlatforms[i] = roundHeight;
				break;
			}
		}
	}
	ReplyToCommand(client, "Bounce Platform Registered");
	return Plugin_Handled;
	
}

//Removes the users registered bounce
public Action:Command_BounceAlertDisable(client, args){
	//duh
	if( !client ){
		ReplyToCommand(client, "[SM] Cannot check bounces from rcon");
		return Plugin_Handled;
	}
	
	new i;
	new id = GetClientUserId(client);
	
	//Removes the user and the height that they registered
	for(i = 0; i < playersLength;i++){
		if(players[i]==id){
			players[i] = 0;
			bouncePlatforms[i] = 0;
			break;
		}
	}
	
	ReplyToCommand(client, "Bounce Alert Disabled!")
	return Plugin_Handled;
}



//I stole this code from some guy on a forum and it works pretty well
//Pretty much just gets where you are looking and I used it everywhere
Float:GetEndPosition(client, bool:straightDown){
	decl Float:start[3], Float:angle[3], Float:end[3];
	GetClientEyePosition(client, start);
	if(straightDown){
		angle[0] = 90.0;
	}else{
		GetClientEyeAngles(client, angle);
	}
	TR_TraceRayFilter(start, angle, MASK_SOLID, RayType_Infinite, TraceEntityFilterPlayer, client);
	if (TR_DidHit(INVALID_HANDLE)){
		TR_GetEndPosition(end, INVALID_HANDLE);
	}
	return end[2];
}

//I have no idea what this does I stole this too
public bool:TraceEntityFilterPlayer(entity, contentsMask, any:data){
	return entity > MaxClients;
}  

